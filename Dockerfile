FROM ruby
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev && apt-get install -y nodejs && apt-get install -y vim 
RUN mkdir /myapp
WORKDIR /myapp
RUN gem install rails && gem install bundler