#! /bin/bash -ilex
docker run -itd --name db -v /mnt/data_db:/var/lib/ -e POSTGRES_PASSWORD="123456" postgres
docker run -v ${WORKSPACE}:/myapp -itd -w /myapp --link db -p 3000:3000 evn/rails:v1 sh -c "bundle install && rake db:create && rake db:migrate && rails s "
